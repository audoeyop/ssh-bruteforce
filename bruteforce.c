#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int getPassword(char *str, int total_len){
  //!/ABCDEFGHIJKLMNOPQRSTUVWXYZ
  //abcdefghijklmnopqrstuvwxyz
  //0123456789
  char characters[] = "abcdefghijklmnopqrstuvwxyz1234567890"; 
  int char_length;
  char_length = strlen(characters);

  int str_length, i, j, k, l;
  str_length = strlen(str);

  if(str_length >= total_len) return 0;

  for(i=0; i<str_length; i++){
    int break_loop = 0;
    for(l=0; l<char_length;l++){

      if(l < (char_length - 1)){
        if(str[i] == characters[l]){str[i] = characters[l+1];break_loop = 1;break;}
      }
      if(l == (char_length - 1)){
        if(i > 0){
          for(k=0; k<=(i); k++){
            str[k] = characters[0];
          }
        }else{
          str[0] = characters[0];
        }
      }
    }
    if(break_loop == 1){
      break;
    }
  }
  if(i == str_length){
    for(j=0; j<str_length; j++){
      str[j] = characters[0];
    }
    str[i] = characters[0];
    str[i+1] = '\0';
  }
  return 1;
}

int main(int argc, char ** argv){
  char str[255] = "";

  if(argc > 1)  
    strcat(str ,argv[1]);

  getPassword(&str, 15);
  printf(str);

  return 0;
}
