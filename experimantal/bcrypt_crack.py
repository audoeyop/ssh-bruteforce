#!/usr/bin/python
import paramiko, subprocess, os, threading, datetime, time, signal, string, bcrypt
hash = "$2a$05$xWfTN46qG5P8wgfq/xZRH.SKU/wr7MUInBeHN4wZBlems30XiSPuW"
#hash = "$2a$05$.tJf1P7WLSydXyTlEIfU9uQDl..qFh6rwibsEoLKbbiAUToPtbYsS"
password = "aaaaaa"
found_password = False
threads = 30

def getPassword(str):
  str = list(str)
  characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  char_length = len(characters)
  str_length = len(str)

  #if(str_length >= total_len):
  #  return 0

  for i in range(str_length):
    break_loop = 0

    for l in range(char_length):

      if(l < (char_length - 1)):
        if(str[i] == characters[l]):
          str[i] = characters[l+1]
          break_loop = 1
          break

      if(l == (char_length - 1)):
        if(i > 0):
          for k in range(i+1):
            str[k] = characters[0]
        else:
          str[0] = characters[0]

    if(break_loop == 1):
      break

    if(i == (str_length - 1)):
      for j in range(str_length):
        str[j] = characters[0]

      str.append(characters[0])

  str = ''.join(str)
  return str

class ThreadClass(threading.Thread):
  def __init__(self):
    threading.Thread.__init__(self)
  
  def run(self):
    global threads
    global password
    global found_password

  while (found_password == False):
    success = True
    this_password = password
    print "trying:'" + this_password + "'"

    password = getPassword(password)

    if(bcrypt.hashpw(this_password, hash) != hash):
      success = False
 
    if (success == True):
      print "Password is: " + this_password
      print ''
      found_password = True

print 'Begining bruteforce'

for i in range(threads):
    t = ThreadClass()
    t.start()
