# SSH Bruteforce
SSH bruteforce script, given a valid username

## Files
### bruteforce.c
Helper program that generates subsequent enumeration of strings that 
`bruteforce.pyc` uses to attempt ssh authentication

### bruteforce.py
Script that generates ssh authentication requests in order to attempt an ssh bruteforce attack